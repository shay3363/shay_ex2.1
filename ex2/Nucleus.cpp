#include "Nucleus.h"


void Nucleus::init(const std::string dna_sequence)
{
	this->_DNA_strand = "";
	this->_DNA_strand = dna_sequence;
	this->_complementary_DNA_strand = "";
	for (int i = 0; i < dna_sequence.size(); i++)
	{
		if (dna_sequence[i] == 'T')
			this->_complementary_DNA_strand += 'A';
		else if (dna_sequence[i] == 'A')
		{
			this->_complementary_DNA_strand +=  'T';
		}
		else if (dna_sequence[i] == 'G')
		{
			this->_complementary_DNA_strand += 'C';
		}
		else if (dna_sequence[i] == 'C')
		{
			this->_complementary_DNA_strand +='G';
		}
		else  //if this isnt one of the letters it isnt a valid dna strand.
		{
			
			std::cerr << "dna sequence cant contain letters other than: A,T,G,C";
			_exit(1);
		}

	}
}
std::string Nucleus::get_RNA_transcript(const Gene& gene) const
{
	std::string rna = "";
	if (gene.is_on_complementary_dna_strand())
		rna = this->_complementary_DNA_strand;
	else
	{
		rna = this->_DNA_strand;
	}
	std::replace(rna.begin(), rna.end(), 'T', 'U'); //function from algoritm header that replaces occurences of one to to another.
	return rna;
}

std::string Nucleus::get_reversed_DNA_strand() const
{
	std::string dna = this->_DNA_strand;
	std::reverse(dna.begin(), dna.end());
	return dna;
}

unsigned int Nucleus::get_num_of_codon_appearances(const std::string& codon) const
{
	const size_t step = codon.size();

	size_t count(0);
	size_t pos(0);

	while ((pos = this->_DNA_strand.find(codon, pos)) != std::string::npos)
	{
		pos += step;
		++count;
	}

	return count;
}


void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	this->_start = start;
	this->_end = end;
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}

unsigned int Gene::getStart() const
{
	return this->_start;
}

unsigned int Gene::getEnd() const
{
	return this->_end;
}

bool Gene::is_on_complementary_dna_strand() const
{
	return this->_on_complementary_dna_strand;
}

void Gene::setStart(const unsigned int start)
{
	this->_start = start;
}

void Gene::setEnd(const unsigned int end)
{
	this->_end = end;
}

void Gene::setComplementary(const bool on_complementary_dna_strand)
{
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}