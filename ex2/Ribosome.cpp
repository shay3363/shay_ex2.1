#include "Ribosome.h"


Protein* Ribosome::create_protein(std::string &RNA_transcript) const
{
	Protein* prot = new Protein;
	prot->init();
	while (RNA_transcript.size() >= 3)
	{
		std::string aminoCodon = RNA_transcript.substr(0, 3);
		RNA_transcript = RNA_transcript.substr(3, RNA_transcript.size() -1);
		AminoAcid acid = get_amino_acid(aminoCodon);
		if (aminoCodon == "unknown")
		{
			prot->clear();
			prot = NULL;
			return prot;
		}
		else
		{
			prot->add(acid);
		}
	}
	return prot;
}