#include "Mitochondrion.h"

void Mitochondrion::init()
{
	this->_glocuse_level = 0;
	this->_has_glocuse_receptor = false;
}

void Mitochondrion::insert_glucose_receptor(const Protein & protein)
{
	bool flag = true;
	AminoAcidNode* amino;
	amino = protein.get_first();
	int amino_acids[7] = { ALANINE, LEUCINE , GLYCINE , HISTIDINE, LEUCINE, PHENYLALANINE, AMINO_CHAIN_END };
	int i = 0;
	while (amino->get_next())//going until the end of the list
	{
		if (amino->get_data() != amino_acids[i])//if the amino acid doesnt match the order then it cant have a receptor
		{
			flag = false;
		}
		i++;
		amino = amino->get_next();
	}
	this->_has_glocuse_receptor = flag;
}

void Mitochondrion::set_glucose(const unsigned int glocuse_units)
{
	this->_glocuse_level = glocuse_units;
}

bool Mitochondrion::produceATP() const
{
	bool ret = false;
	if (this->_glocuse_level >= 50 && this->_has_glocuse_receptor)
		ret = true;
	return true;
}
